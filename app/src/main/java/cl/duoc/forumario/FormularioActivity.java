package cl.duoc.forumario;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FormularioActivity extends AppCompatActivity {

    private EditText txtNombre, txtCelular, txtCorreo, txtComentario;
    private Button btnAgregar, btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtCelular = (EditText)findViewById(R.id.txtCelular);
        txtCorreo = (EditText)findViewById(R.id.txtCorreo);
        txtComentario = (EditText)findViewById(R.id.txtComentario);
        btnAgregar = (Button)findViewById(R.id.btnAgregar);
        btnLimpiar = (Button)findViewById(R.id.btnLimpiar);

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCajasDeTexto();
            }
        });

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agregarContacto();
            }
        });
    }

    private void agregarContacto() {
        if(isValueTxt(txtNombre)){
            txtNombre.setError("Ingrese nombre");
            txtNombre.requestFocus();
        }else if(isValueTxt(txtCelular)){
            txtCelular.setError("Ingrese nombre");
            txtCelular.requestFocus();
        }else if(isValueTxt(txtCorreo) || !validateEmail(txtCorreo.getText().toString())){
            txtCorreo.setError("Ingrese correo valido");
            txtCorreo.requestFocus();
        }else if(isValueTxt(txtComentario)){
            txtComentario.setError("Ingrese nombre");
            txtComentario.requestFocus();
        }else{

            Toast.makeText(FormularioActivity.this, "Comentario enviado", Toast.LENGTH_SHORT).show();
            limpiarCajasDeTexto();
        }
    }

    private boolean isValueTxt(EditText txt){
        return txt.getText().toString().length()<1;
    }


    private void limpiarCajasDeTexto() {
        txtNombre.setText("");
        txtCelular.setText("");
        txtCorreo.setText("");
        txtComentario.setText("");
        txtNombre.requestFocus();
    }


    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean validateEmail(String email) {

        // Compiles the given regular expression into a pattern.
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);

        // Match the given input against this pattern
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();

    }
}
